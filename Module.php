<?php

namespace hoopy1986\user;

use Yii;
use yii\helpers\ArrayHelper;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'hoopy1986\user\controllers';

    public $urlPrefix = 'user';

    public $userModel = 'app\models\User';
    public $userSearchModel = 'app\models\search\SearchUser';
    public $userLoginAttemptModel = 'app\models\UserLoginAttempt';
    public $userLoginAttemptSearchModel = 'app\models\search\SearchUserLoginAttempt';

    public $userAuthItemModel = 'hoopy1986\user\models\AuthItem';
    public $userAuthItemSearchModel = 'hoopy1986\user\models\search\SearchAuthItem';

    public $userProfileAttached = [];

    public $adminCanCreateUser = true;
    public $allowRegistration = true;
    public $passwordStrength = 2;
    public $passwordMinLength = 6;
    public $passwordMaxLength = 20;
    public $activeUsersOnly = true;
    public $disabledUsers = true;
    public $emailVerification = true;
    public $userMailEndpoint = null;

    // default /user/users/view
    public $userViewProfileLink = null;
    // default /user/users/update
    public $userUpdateProfileLink = null;

    public $views = [];

    public function init()
    {
        parent::init();
        //Yii::$app->errorHandler->errorAction = 'admin/default/error';
        $this->views = $this->getCustomViews();
        Yii::$app->i18n->translations['user_components'] =
        Yii::$app->i18n->translations['user_forms'] =
        Yii::$app->i18n->translations['user_controller_user'] =
        Yii::$app->i18n->translations['user_controller_site'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@hoopy1986/user/messages',
        ];


        if ($this->userMailEndpoint === null) {
            if(isset(Yii::$app->params['domainName'])){
                $this->userMailEndpoint = Yii::$app->params['domain'] . '/' . $this->urlPrefix . '/site/';
            }else {
                $this->userMailEndpoint = Yii::$app->getUrlManager()->getBaseUrl(true) . '/' . $this->urlPrefix . '/site/';
            }
        }
        if ($this->userViewProfileLink === null) {
            $this->userViewProfileLink = '/' . $this->urlPrefix . '/users/view';
        }
        if ($this->userUpdateProfileLink === null)
            $this->userUpdateProfileLink = '/' . $this->urlPrefix . '/users/update';
    }

    public function getProfileModels()
    {
        return ArrayHelper::merge(
                          [
                              'profile' => [
                                  'profile' => true,
                                  'label' => Yii::t('user_controller_user', 'Profile'),
                                  'type' => 'detailView',
                                  'model' => $this->userModel,
                                  'view' => '@hoopy1986/user/views/users/profileView',
                                  'update' => '@hoopy1986/user/views/users/profileUpdate'
                              ],
                              'login' => [
                                  'label' => Yii::t('user_controller_user', 'Login attempts'),
                                  'type' => 'gridView',
                                  'searchModel' => $this->userLoginAttemptSearchModel,
                                  'view' => '@hoopy1986/user/views/users/loginAttemptView'
                              ]
                          ],
                              $this->userProfileAttached
        );
    }

    public function getCustomViews()
    {
        return ArrayHelper::merge(
                          [
                              'layout' => null,

                              //TODO DELETE
                              'authorizement' => [
                                  'layout' => null,
                                  'index' => 'index'
                              ],

                              'site' => [
                                  'layout' => null,
                                  'login' => 'login',
                                  'recover' => 'recover',
                                  'recovery' => 'recovery',
                                  'register' => 'register'
                              ],
                              'users' => [
                                  'layout' => null,
                                  'index' => 'index',
                                  'update' => 'update',
                                  'view' => 'view',
                                  'create' => 'create'
                              ],

                              'assignments' => [
                                  'layout' => null,
                                  'index' => 'index'
                              ],
                              'permissions' => [
                                  'layout' => null,
                                  'index' => 'index'
                              ],
                              'roles' => [
                                  'layout' => null,
                                  'index' => 'index',
                                  'view' => 'view',
                                  'create' => 'create',
                                  'update' => 'update'
                              ],
                              'routes' => [
                                  'layout' => null,
                                  'index' => 'index'
                              ],
                              'rules' => [
                                  'layout' => null,
                                  'index' => 'index'
                              ],
                          ],
                              $this->views
        );
    }
}
