<?php
return [
    'sourcePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'messagePath' => dirname(__FILE__),
    'languages' => ['hu'],
    'fileTypes' => ['php'],
    'overwrite' => true,
    'exclude' => [
        '.svn',
        '.git',
        '.gitignore',
        'yiilite.php',
        'yiit.php',
        'yiic.php',
        '/messages',
        '/tests',
        '/migrations',
        '/extensions',
    ],
];
