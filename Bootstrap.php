<?php

namespace hoopy1986\user;

use Yii;
use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

class Bootstrap implements BootstrapInterface
{
    private $urlRules = [
        'user/site/verify/<activationKey>/<username>' => 'user/site/verify',
        'user/site/recover/<activationKey>/<username>' => 'user/site/recover'
    ];

    public function bootstrap($app){
            $app->get('urlManager')->addRules($this->urlRules, false);
    }

}