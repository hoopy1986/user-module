<?php

namespace hoopy1986\user\controllers;

use Yii;
use yii\filters\AccessControl;
use hoopy1986\user\components\BaseController;
use hoopy1986\user\forms\LoginForm;
use hoopy1986\user\forms\RegisterForm;
use hoopy1986\user\forms\RecoveryForm;
use hoopy1986\user\forms\PasswordForm;

class SiteController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'register', 'recovery', 'verify', 'recover'],
                'rules' => [
                    [
                        'actions' => ['login', 'register', 'recovery', 'verify', 'recover'],
                        'allow' => true
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@']
                    ]
                ],
            ]
        ];
    }

    public function actionLogin()
    {
        $this->guestGoHome();
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = Yii::$app->user->getIdentity();
            Yii::$app->user->id;
            $user->id;
            if($user->hasMethod('afterLogin'))
                $user->afterLogin();
            return $this->goBack();
        } else {
            return $this->baseRender(['model' => $model]);
        }
    }

    public function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->getIdentity();
            if($user->hasMethod('beforeLogout'))
                $user->beforeLogout();
            Yii::$app->user->logout();
        }
        return $this->goHome();
    }

    public function actionRecovery()
    {
        $this->guestGoHome();

        $model = new RecoveryForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->recoveryUser();
            return $this->goHome();
        }

        return $this->baseRender(['model' => $model]);
    }

    public function actionRegister()
    {
        if (!$this->module->allowRegistration) {
            return $this->goHome();
        }
        $this->guestGoHome();

        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->registerUser();
            return $this->goHome();
        }

        return $this->baseRender(['model' => $model]);
    }

    public function actionVerify($activationKey, $username)
    {
        $userClass = Yii::$app->controller->module->userModel;
        $user = $userClass::findByUsername($username);
        if(!$user || !$user->verifyActivationKey($activationKey)){
            Yii::$app->session->setFlash('error', Yii::t('user_controller_site', 'Wrong username or key!'));
        }
        else{
            $user->email_verified = true;
            $user->activation_key = Yii::$app->security->generateRandomString();
            $user->update();
            Yii::$app->session->setFlash('success', Yii::t('user_controller_site', 'The email address is verified. Now You can log in!'));
        }
        $this->goHome();
    }

    public function actionRecover($activationKey, $username)
    {
        $userClass = Yii::$app->controller->module->userModel;
        $user = $userClass::findByUsername($username);
        if(!$user || !$user->verifyActivationKey($activationKey)){
            Yii::$app->session->setFlash('error', Yii::t('user_controller_site', 'Wrong username or key!'));
            $this->goHome();
        }
        else{
            $model = new PasswordForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user->email_verified = true;
                $user->activation_key = Yii::$app->security->generateRandomString();
                $user->setPassword($model->password);
                $user->update();
                Yii::$app->session->setFlash('success', Yii::t('user_controller_site', 'The password has changed!'));
                return $this->goHome();
            }

            return $this->baseRender(['model' => $model]);
        }
    }

    private function guestGoHome(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
    }
} 