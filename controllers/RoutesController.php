<?php


namespace hoopy1986\user\controllers;

use Yii;
use yii\caching\TagDependency;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use hoopy1986\user\components\BaseController;
use hoopy1986\user\components\Cache;

class RoutesController extends BaseController
{

    const CACHE_TAG = 'hoopy1986.user.route';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                    'remove' => ['post'],
                    'routesearch' => ['post']
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $authManager = Yii::$app->getAuthManager();
        $exists = $existsOptions = $routes = [];
        foreach ($this->getAppRoutes() as $route) {
            $routes[$route] = $route;
        }
        $allRoutes = $routes;
        foreach ($authManager->getPermissions() as $name => $permission) {
            if ($name[0] !== '/') {
                continue;
            }
            $exists[$name] = $name;
            if (isset($allRoutes[$name])) {
                unset($routes[$name]);
            } else {
                $r = explode('&', $name);
                if (!isset($allRoutes[$r[0]])) {
                    $existsOptions[$name] = ['class' => 'lost'];
                }
            }
        }
        return $this->baseRender(['routes' => $routes, 'exists' => $exists, 'existsOptions' => $existsOptions]);
    }

    public function actionAdd()
    {
        $authManager = Yii::$app->getAuthManager();
        $options = Yii::$app->request->post('options');
        foreach ($options as $option) {
            $route = $authManager->createPermission($option);
            $route->description = 'Route';
            $authManager->add($route);
        }
    }

    public function actionRemove()
    {
        {
            $authManager = Yii::$app->getAuthManager();
            $options = Yii::$app->request->post('options');
            foreach ($options as $option) {
                $route = $authManager->getPermission($option);
                $authManager->remove($route);
            }
        }
    }

    public function actionRoutesearch()
    {
        $this->invalidate();
    }

    public function getAppRoutes()
    {
        $key = __METHOD__;
        $cache = Cache::instance()->cache;
        if ($cache === null || ($result = $cache->get($key)) === false) {
            $result = [];
            $this->getRouteRecrusive(Yii::$app, $result);
            if ($cache !== null) {
                $cache->set(
                      $key,
                          $result,
                          Cache::instance()->cacheDuration,
                          new TagDependency([
                              'tags' => self::CACHE_TAG
                          ])
                );
            }
        }
        return $result;
    }

    private function getRouteRecrusive($module, &$result)
    {
        $token = "Get Route of '" . get_class($module) . "' with id '" . $module->uniqueId . "'";
        Yii::beginProfile($token, __METHOD__);
        try {
            foreach ($module->getModules() as $id => $child) {
                if (($child = $module->getModule($id)) !== null) {
                    $this->getRouteRecrusive($child, $result);
                }
            }
            foreach ($module->controllerMap as $id => $type) {
                $this->getControllerActions($type, $id, $module, $result);
            }
            $namespace = trim($module->controllerNamespace, '\\') . '\\';
            $this->getControllerFiles($module, $namespace, '', $result);
            $result[] = ($module->uniqueId === '' ? '' : '/' . $module->uniqueId) . '/*';
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    private function getControllerFiles($module, $namespace, $prefix, &$result)
    {
        $path = @Yii::getAlias('@' . str_replace('\\', '/', $namespace));
        $token = "Get controllers from '$path'";
        Yii::beginProfile($token, __METHOD__);
        try {
            if (!is_dir($path)) {
                return;
            }
            foreach (scandir($path) as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (is_dir($path . '/' . $file)) {
                    $this->getControllerFiles($module, $namespace . $file . '\\', $prefix . $file . '/', $result);
                } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                    $id = Inflector::camel2id(substr(basename($file), 0, -14));
                    $className = $namespace . Inflector::id2camel($id) . 'Controller';
                    if (strpos($className, '-') === false && class_exists($className) && is_subclass_of(
                            $className,
                            'yii\base\Controller'
                        )
                    ) {
                        $this->getControllerActions($className, $prefix . $id, $module, $result);
                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    private function getControllerActions($type, $id, $module, &$result)
    {
        $token = "Create controller with config=" . VarDumper::dumpAsString($type) . " and id='$id'";
        Yii::beginProfile($token, __METHOD__);
        try {
            /* @var $controller \yii\base\Controller */
            $controller = Yii::createObject($type, [$id, $module]);
            $this->getActionRoutes($controller, $result);
            $result[] = '/' . $controller->uniqueId . '/*';
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }

    private function getActionRoutes($controller, &$result)
    {
        $token = "Get actions of controller '" . $controller->uniqueId . "'";
        Yii::beginProfile($token, __METHOD__);
        try {
            $prefix = '/' . $controller->uniqueId . '/';
            foreach ($controller->actions() as $id => $value) {
                $result[] = $prefix . $id;
            }
            $class = new \ReflectionClass($controller);
            foreach ($class->getMethods() as $method) {
                $name = $method->getName();
                if ($method->isPublic() && !$method->isStatic() && strpos(
                        $name,
                        'action'
                    ) === 0 && $name !== 'actions'
                ) {
                    $result[] = $prefix . Inflector::camel2id(substr($name, 6));
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
        Yii::endProfile($token, __METHOD__);
    }


    protected function invalidate()
    {
        if (Cache::instance()->cache !== null) {
            TagDependency::invalidate(Cache::instance()->cache, self::CACHE_TAG);
        }
    }
} 