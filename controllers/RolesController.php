<?php


namespace hoopy1986\user\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\rbac\Item;
use hoopy1986\user\components\BaseController;

class RolesController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'addchild' => ['post'],
                    'removechild' => ['post']
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $userAuthItemSearchModel = $this->module->userAuthItemSearchModel;
        $searchModel = new $userAuthItemSearchModel(['type' => Item::TYPE_ROLE]);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->baseRender(
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }

    public function actionView($id)
    {
        $authManager = Yii::$app->getAuthManager();

        $model = $authManager->getRole($id);

        $roles = [];
        $attachedRoles = [];
        $routes = [];
        $attachedRoutes = [];
        $permissions = [];
        $attachedPermissions = [];

        foreach ($authManager->getRoles() as $name => $role) {
            if ($name !== $model->name) {
                $roles[$name] = $name;
            }
        }

        foreach ($authManager->getPermissions() as $name => $permission) {
            if ($name[0] !== '/') {
                $permissions[$name] = $name;
            }else{
                $routes[$name] = $name;
            }
        }

        foreach ($authManager->getRolesByRoleNotRecursive($id) as $name => $role) {
            if (isset($roles[$name])) {
                unset($roles[$name]);
                $attachedRoles[$name] = $name;
            }
        }

        foreach ($authManager->getPermissionsByRoleNotRecursive($id) as $name => $permission) {
            if ($name[0] !== '/') {
                if (isset($permissions[$name])) {
                    unset($permissions[$name]);
                    $attachedPermissions[$name] = $name;
                }
            }else{
                if (isset($routes[$name])) {
                    unset($routes[$name]);
                    $attachedRoutes[$name] = $name;
                }
            }
        }

        return $this->baseRender(['model' => $model, 'roles' => $roles, 'attachedRoles' => $attachedRoles, 'routes' => $routes, 'attachedRoutes' => $attachedRoutes,  'permissions' => $permissions, 'attachedPermissions' => $attachedPermissions]);
    }

    public function actionCreate()
    {
        $userAuthItemModel = $this->module->userAuthItemModel;
        $model = new $userAuthItemModel();
        $model->type = Item::TYPE_ROLE;
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        }
        return $this->baseRender(['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $userAuthItemModel = $this->module->userAuthItemModel;
        $model = $userAuthItemModel::findOne(['name' => $id]);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        }
        return $this->baseRender(['model' => $model]);
    }

    public function actionDelete($id)
    {
        $authManager = Yii::$app->authManager;
        $authManager->remove($authManager->getRole($id));
        return $this->redirect('index');
    }

    public function actionAddchild($id, $type = Item::TYPE_PERMISSION)
    {
        $authManager = Yii::$app->getAuthManager();
        $role = $authManager->getRole($id);
        $options = Yii::$app->request->post('options');
        foreach ($options as $option) {
            if($type == Item::TYPE_PERMISSION)
                $child = $authManager->getPermission($option);
            else
                $child = $authManager->getRole($option);
            $authManager->addChild($role, $child);
        }
    }

    public function actionRemovechild($id, $type = Item::TYPE_PERMISSION)
    {
        $authManager = Yii::$app->getAuthManager();
        $role = $authManager->getRole($id);
        $options = Yii::$app->request->post('options');
        foreach ($options as $option) {
            if($type == Item::TYPE_PERMISSION)
                $child = $authManager->getPermission($option);
            else
                $child = $authManager->getRole($option);
            $authManager->removeChild($role, $child);
        }
    }
}