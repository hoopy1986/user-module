<?php


namespace hoopy1986\user\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\rbac\Item;
use hoopy1986\user\components\BaseController;

class PermissionsController extends BaseController{

    public function actionIndex()
    {
        return $this->baseRender();
    }
} 