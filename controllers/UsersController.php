<?php

namespace hoopy1986\user\controllers;

use hoopy1986\user\models\AuthItem;
use Yii;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use hoopy1986\user\components\BaseController;
use hoopy1986\user\components\interfaces\BaseUserInterface;

class UsersController extends BaseController
{
    public function behaviors()
    {
        return [];
    }

    public function actionCreate()
    {
        $userModel = Yii::$app->controller->module->userModel;
        $model = new $userModel(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->is_active = true;
            $model->insert();
            $model->sendEmail('create');
            if($model->hasMethod('afterAdminCreate'))
                $model->afterAdminCreate();
            Yii::$app->session->setFlash('success', Yii::t('user_controller_user', 'The user has been created and mail was sent.'));
            return $this->redirect('index');
        }
        return $this->baseRender(['model' => $model]);
    }

    public function actionIndex()
    {
        $userSearchClass = Yii::$app->controller->module->userSearchModel;

        $searchModel = new $userSearchClass();
        $searchModel->is_disabled = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->baseRender(['model' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionView($id, $tab = null)
    {
        $array = Yii::$app->controller->module->getProfileModels();
        $items = [];
        $model = null;

        foreach($array as $element){
            $attachedModel = null;
            switch($element['type']){
                case 'detailView':
                    $attachedModel = $element['model'];
                    if(!isset($element['profile'])){
                        $attachedModel = $attachedModel::find()->where(['user_id' => $id])->one();
                        $params = ['model' => $attachedModel];
                    }else{
                        $model = $attachedModel::findOne($id);
                        $params = ['model' => $model];
                    }
                    break;
                case 'gridView':
                    $attachedModel = $element['searchModel'];
                    $searchModel = new $attachedModel();
                    $searchModel->user_id = $id;
                    $dataProvider = $searchModel->search(Yii::$app->request->get());
                    $params = ['model' => $searchModel, 'dataProvider' => $dataProvider];
                    break;
                default:
                    break;
            }
            if($attachedModel){
                $items[] = [
                    'label' => $element['label'],
                    'content' => $this->renderPartial($element['view'], $params),
                ];
            }
        }

        return $this->baseRender(['model' => $model, 'items' => $items]);
    }

    public function actionUpdate($id)
    {
        $post = Yii::$app->request->post();
        $array = Yii::$app->controller->module->getProfileModels();
        $items = [];
        $model = null;

        foreach ($array as $element) {
            $attachedModel = null;
            $active = false;
            if($element['type'] === 'detailView'){
                $attachedModelClassName = $element['model'];
                $attachedModel = $attachedModelClassName;
                if (!isset($element['profile'])) {
                    $attachedModel = $attachedModel::find()->where(['user_id' => $id])->one();
                    if(!$attachedModel){
                        $attachedModel = new $attachedModelClassName;
                        $attachedModel->user_id = $id;
                    }
                } else {
                    $attachedModel = $attachedModel::findOne($id);
                    $model = $attachedModel;
                }
                if ($attachedModel) {
                    $params = ['model' => $attachedModel];
                    if(isset($post['modelName'])){
                        if($attachedModel::className() == $post['modelName']){
                            $active = true;
                            if ($attachedModel->load(Yii::$app->request->post()) && $attachedModel->validate()) {
                                $attachedModel->save();
                                Yii::$app->session->setFlash('success', Yii::t('user_controller_user', 'The model has updated successfully!'));
                            }else{
                                Yii::$app->session->setFlash('error', Yii::t('user_controller_user', 'The model has not updated!'));
                            }
                        }
                    }
                    $params['name'] = $attachedModel::className();
                    $items[] = [
                        'label' => $element['label'],
                        'content' => $this->renderPartial($element['update'], $params),
                        'active' => $active
                    ];
                }
            }
        }
        return $this->baseRender(['model' => $model, 'items' => $items]);
    }

    public function actionVerify($id)
    {
        $this->loadModel($id)->toggleStatus(BaseUserInterface::STATUS_EMAIL_VERIFIED);
    }

    public function actionActivate($id)
    {
        $this->loadModel($id)->toggleStatus(BaseUserInterface::STATUS_IS_ACTIVE);
    }

    public function actionDisable($id)
    {
        $this->loadModel($id)->toggleStatus(BaseUserInterface::STATUS_IS_DISABLED);
    }

    public function actionSetrole($id){
        $user = $this->loadModel($id);
        $user->setRole(Yii::$app->request->post('role'));
        echo \yii\helpers\Json::encode(['output' => '', 'message'=>'']);
    }

    private function loadModel($id){
        $userClass = Yii::$app->controller->module->userModel;
        return $userClass::findOne($id);
    }
}
