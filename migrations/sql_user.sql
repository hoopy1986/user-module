drop table if exists `user_remote_identity`;
drop table if exists `user_used_password`;
drop table if exists `user_profile_picture`;
drop table if exists `user_login_attempt`;
drop table if exists `user`;

CREATE TABLE `user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `title` varchar(255) DEFAULT NULL,
    `firstname` varchar(255) DEFAULT NULL,
    `lastname` varchar(255) DEFAULT NULL,
    `auth_key` varchar(255) DEFAULT NULL,
    `activation_key` varchar(255) DEFAULT NULL,
    `access_token` varchar(255) DEFAULT NULL,
    `created_on` timestamp NULL DEFAULT NULL,
    `updated_on` timestamp NULL DEFAULT NULL,
    `last_visited_on` timestamp NULL DEFAULT NULL,
    `password_set_on` timestamp NULL DEFAULT NULL,
    `email_verified` tinyint(1) NOT NULL DEFAULT '0',
    `is_active` tinyint(1) NOT NULL DEFAULT '0',
    `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
)  ENGINE=InnoDB AUTO_INCREMENT=1;
CREATE UNIQUE INDEX `user_username_idx` ON `user` (`username`); 
CREATE UNIQUE INDEX `user_email_idx` ON `user` (`email`); 
CREATE INDEX `user_email_verified_idx` ON `user` (`email_verified`); 
CREATE INDEX `user_is_active_idx` ON `user` (`is_active`); 
CREATE INDEX `user_is_disabled_idx` ON `user` (`is_disabled`);

CREATE TABLE `user_remote_identity` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `provider` varchar(100) NOT NULL,
    `identifier` varchar(100) NOT NULL,
    `created_on` timestamp NOT NULL,
    `last_used_on` timestamp,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) references `user` (`id`) on delete cascade on update cascade
)  ENGINE=InnoDB AUTO_INCREMENT=1;
CREATE UNIQUE INDEX `user_remote_identity_user_id_provider_identifier_idx` ON `user_remote_identity` (`user_id`, `provider`, `identifier`); 
CREATE INDEX `user_remote_identity_user_id_idx` ON `user_remote_identity` (`user_id`);

CREATE TABLE `user_used_password` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `password` varchar(255) NOT NULL,
    `set_on` timestamp NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) references `user` (`id`) on delete cascade on update cascade
)  ENGINE=InnoDB AUTO_INCREMENT=1;
CREATE INDEX `user_used_password_user_id_idx` ON `user_used_password` (`user_id`);

CREATE TABLE `user_profile_picture` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `original_picture_id` int(11) NOT NULL,
    `filename` varchar(255) NOT NULL,
    `width` int(11) NOT NULL,
    `height` int(11) NOT NULL,
    `mimetype` varchar(255) NOT NULL,
    `created_on` timestamp NOT NULL,
    `contents` text NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) references `user` (`id`) on delete cascade on update cascade,
    FOREIGN KEY (`original_picture_id`) references `user_profile_picture` (`id`) on delete cascade on update cascade
)  ENGINE=InnoDB AUTO_INCREMENT=1;
CREATE INDEX `user_profile_picture_user_id_idx` ON `user_profile_picture` (`user_id`); 
CREATE INDEX `user_profile_picture_original_picture_id_idx` ON `user_profile_picture` (`original_picture_id`); 
CREATE INDEX `user_profile_picture_width_height_idx` ON `user_profile_picture` (`width`, `height`);

CREATE TABLE `user_login_attempt` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `username` varchar(255) NOT NULL,
    `performed_on` timestamp NOT NULL,
    `is_successful` tinyint(1) NOT NULL DEFAULT '0',
    `session_id` varchar(255),
    `ipv4` int(11),
    `user_agent` varchar(255),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) references `user` (`id`) on delete cascade on update cascade
)  ENGINE=InnoDB AUTO_INCREMENT=1;
CREATE INDEX `user_login_attempt_user_id_idx` ON `user_login_attempt` (`user_id`); 
