<?php

use yii\db\Schema;
use hoopy1986\user\migrations\Migration;

class m150624_000001_init_user extends Migration
{
    public function up()
    {
        $this->execute(
            file_get_contents(
                Yii::getAlias('@hoopy1986/user/migrations/sql_user.sql')
            )
        );
    }

    public function down()
    {
        $this->dropTable('{{%user_remote_identity}}');
        $this->dropTable('{{%user_used_password}}');
        $this->dropTable('{{%user_profile_picture}}');
        $this->dropTable('{{%user_login_attempt}}');
        $this->dropTable('{{%user}}');
    }
}