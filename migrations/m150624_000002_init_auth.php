<?php

use yii\db\Schema;
use hoopy1986\user\migrations\Migration;

class m150624_000002_init_auth extends Migration
{
    public function up()
    {
        $this->execute(
            file_get_contents(
                Yii::getAlias('@hoopy1986/user/migrations/sql_auth.sql')
            )
        );
    }

    public function down()
    {
        $this->dropTable('{{%user_auth_assignment}}');
        $this->dropTable('{{%user_auth_item_child}}');
        $this->dropTable('{{%user_auth_item}}');
        $this->dropTable('{{%user_auth_rule}}');
    }
}