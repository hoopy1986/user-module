<?php

namespace hoopy1986\user\widgets;

use hoopy1986\user\widgets\assets\DualListBoxAsset;
use Yii;
use yii\helpers\Json;
use yii\helpers\Html;

class DualListBox extends \yii\base\Widget{

    public $options = [];

    public $clientOptions = [];

    public $clientEvents = [];

    private $tagName = 'select';

    public $multiArray = false;
    public $selectedElements = [];
    public $unSelectedElements = [];
    public $selectedTitle = '';
    public $unSelectedTitle = '';
    public $addUrl = false;
    public $removeUrl = false;
    public $height = '300px';

    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    public function run()
    {
        $this->registerPlugin('DualListBox');
        return Html::tag('div', Html::tag($this->tagName, "", $this->options));
    }

    protected function registerPlugin($name)
    {
        $view = $this->getView();

        DualListBoxAsset::register($view);
        $this->clientOptions = [
            'title' => $this->options['id'],
            'height' => $this->height,
            'json' => false,
            'array' => true,
            'multiArray' => $this->multiArray,
            'selectedElements' => $this->selectedElements,
            'unSelectedElements' => $this->unSelectedElements,
            'addUrl' => $this->addUrl,
            'removeUrl' => $this->removeUrl,
            'selectedTitle' => $this->selectedTitle,
            'unSelectedTitle' => $this->unSelectedTitle,
        ];

        $id = $this->options['id'];

        if ($this->clientOptions !== false) {
            $options = empty($this->clientOptions) ? '' : Json::encode($this->clientOptions);
            $js = "jQuery('#$id').$name($options);";
            $view->registerJs($js);
        }
    }
}