<?php

namespace hoopy1986\user\widgets\assets;

use yii\web\AssetBundle;

class DualListBoxAsset extends AssetBundle
{
    public $sourcePath = '@hoopy1986/user/widgets/assets';

    public $css = [];

    public $js = [
        'js/dual-list-box.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
