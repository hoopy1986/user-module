<?php


namespace hoopy1986\user\components;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class BaseController extends Controller{
    public $views;

    public function baseRender($params = null)
    {
        $this->views = $this->module->views;
        $this->setLayout();
        if($params === null)
            return $this->render($this->getCustomView());
        return $this->render($this->getCustomView(), $params);
    }

    private function getCustomView()
    {
        return $this->views[$this->id][$this->action->id];
    }

    public function setLayout(){
        if($this->views[$this->id]['layout'] !== null)
            $this->layout = $this->views[$this->id]['layout'];
        else if($this->views['layout'] !== null)
            $this->layout = $this->views['layout'];
        else
            return;
    }
} 