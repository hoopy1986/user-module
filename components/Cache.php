<?php


namespace hoopy1986\user\components;

use Yii;
use yii\helpers\ArrayHelper;


class Cache  extends \yii\base\Object
{
    public $cache = 'cache';

    public $cacheDuration = 2592000;

    private static $_instance;

    public function init()
    {
        if ($this->cache !== null && !($this->cache instanceof Cache)) {
            if (is_string($this->cache) && strpos($this->cache, '\\') === false) {
                $this->cache = Yii::$app->get($this->cache, false);
            } else {
                $this->cache = Yii::createObject($this->cache);
            }
        }
        parent::init();
    }

    public static function instance()
    {
        if (self::$_instance === null) {
            $type = ArrayHelper::getValue(Yii::$app->params, 'hoopy1986.user.cache', []);
            if (is_array($type) && !isset($type['class'])) {
                $type['class'] = static::className();
            }
            return self::$_instance = Yii::createObject($type);
        }
        return self::$_instance;
    }
}