<?php

namespace hoopy1986\user\components;

use yii\helpers\Url;
use yii\helpers\Html;

class CustomHtml
{
    public static function ajaxA($text, $url = null, $options = [])
    {
        $options['href'] = '';
        $options['onclick'] = '{
                                if (event){
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                                else if(window.event){
                                   window.event.cancelBubble=true;
                                }';

        if(isset($options['confirm'])){
            $options['onclick'] .= 'if(!confirm("'.$options['confirm'].'"))return;';
        }

        $options['onclick'] .= '$.ajax({';

        if(isset($options['data-method'])){
            $options['onclick'] .= '    method: "' . $options['data-method'] . '",';
        }

        $options['onclick'] .= '    url: "' . Url::to($url) . '",
                                })';
        if(isset($options['containerID'])){
            $options['onclick'] .= '.done(function(){
                    $.pjax.reload({container:"#'.$options['containerID'].'"});
                                })';
        }

        $options['onclick'] .= ';}';

        return Html::tag('a', $text, $options);
    }

} 