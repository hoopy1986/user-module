<?php

namespace hoopy1986\user\components;

use Yii;

class Menu
{

    public static function initLanguage(){
        Yii::$app->i18n->translations['user_components'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@hoopy1986/user/messages',
        ];
    }

    public static function getUserItems()
    {
        self::initLanguage();
        return [
            'label' => Yii::t('user_components','Users'), 'icon' => 'user',  'url' => ['/user']
        ];
    }

    public static function getAuthItems()
    {
        self::initLanguage();
        return [
            'label' => Yii::t('user_components','Authorization'), 'icon' => 'lock', 'items' => [
                ['label' => Yii::t('user_components','Assignment'), 'url' => ['/user/assignments/index']],
                ['label' => Yii::t('user_components','Roles'), 'url' => ['/user/roles/index']],
                ['label' => Yii::t('user_components','Permissions'), 'url' => ['/user/permissions/index']],
                ['label' => Yii::t('user_components','Routes'), 'url' => ['/user/routes/index']],
                ['label' => Yii::t('user_components','Rules'), 'url' => ['/user/rules/index']]
           ]
        ];
    }
}