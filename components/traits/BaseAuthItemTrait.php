<?php

namespace hoopy1986\user\components\traits;

use Yii;

trait BaseAuthItemTrait
{
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['rule_name'], 'in',  'range' => array_keys(Yii::$app->authManager->getRules()), 'message' => Yii::t('authorization', 'Rule not exists')],
                [['name'], 'uniqueItem'],
                [['description', 'data', 'rule_name'], 'default'],
            ]
        );
    }

    public function uniqueItem()
    {
        $dirtyAttrributes = $this->getDirtyAttributes();
        if(!isset($dirtyAttrributes['name']))
            return;
        $authManager = Yii::$app->authManager;
        $value = $this->name;
        if ($authManager->getRole($value) !== null || $authManager->getPermission($value) !== null) {
            $message = Yii::t('authorization', '{attribute} "{value}" has already been taken.');
            $params = [
                'attribute' => $this->getAttributeLabel('name'),
                'value' => $value,
            ];
            $this->addError('name', Yii::$app->getI18n()->format($message, $params, Yii::$app->language));
        }
    }
} 