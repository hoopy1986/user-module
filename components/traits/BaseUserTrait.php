<?php

namespace hoopy1986\user\components\traits;

use Yii;
use yii\helpers\Url;

trait BaseUserTrait {

    public function beforeSave($insert){
        if ($insert) {
            $this->created_on = date('Y-m-d H:i:s');
        }

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_on = date('Y-m-d H:i:s');
                $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
            }
            $this->updated_on = date('Y-m-d H:i:s');
            return true;
        }
        return false;
    }

    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLocalizedUsername(){
        return \Yii::t('user_components', '{firstname} {lastname}', ['firstname' => $this->firstname, 'lastname' => $this->lastname]);
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        try {
            return Yii::$app->security->validatePassword($password, $this->password);
        } catch (\yii\base\InvalidParamException $e) {
            return false;
        }
    }

    public function getActivationKey()
    {
        $this->activation_key = Yii::$app->security->generateRandomString();
        return $this->save(false) ? $this->activation_key : false;
    }

    public function verifyActivationKey($activationKey)
    {
        return $this->activation_key === $activationKey ? true : false;
    }

    public function authenticate(){
        if(Yii::$app->controller->module->disabledUsers){
            if($this->is_disabled){
                return ['username', Yii::t('user_controller_user','The user is disabled')];
            }
        }
        if(Yii::$app->controller->module->activeUsersOnly){
            if(!$this->is_active){
                return ['username', Yii::t('user_controller_user','The user is inactive')];
            }
        }
        if(Yii::$app->controller->module->emailVerification){
            if(!$this->email_verified){
                return ['username', Yii::t('user_controller_user','The user email address is not verified. Check your emails or get a password reminder.')];
            }
        }
        $this->saveLogin();
        $this->last_visited_on = date('Y-m-d H:i:s');
        $this->save(false);
        return true;
    }

    public function setPassword($password, $save = false){
        $this->password = Yii::$app->security->generatePasswordHash($password);
        $this->password_set_on = date('Y-m-d H:i:s');
        if($save){
            $this->save(true);
        }
    }

    public function sendEmail($mode){
        if(Yii::$app->hasModule('userMailer'))
            return;
        $siteUrl = Yii::$app->controller->module->userMailEndpoint;
        switch($mode){
            case 'verify':
                $activationKey = $this->getActivationKey();
                $params['link'] = $siteUrl . $mode . '/' . $activationKey . '/' . $this->username;
                $subject = Yii::t('user_components', 'Email verification');
                break;
            case 'recover':
                $activationKey = $this->getActivationKey();
                $params['link'] = $siteUrl . $mode . '/' . $activationKey . '/' . $this->username;
                $subject = Yii::t('user_components', 'Password recovery');
                break;
            case 'create':
                $activationKey = $this->getActivationKey();
                $params['link'] = $siteUrl . 'recover' . '/' . $activationKey . '/' . $this->username;
                $subject = Yii::t('user_components', 'You have been registered');
                break;
            default:
                return;
        }
        $message = Yii::$app->userMailer->compose($mode, $params);
        $message->setSubject($subject);
        $message->setTo($this->email);
        $message->send();
    }

    public function toggleStatus($status)
    {
        switch ($status) {
            case self::STATUS_EMAIL_VERIFIED: $this->email_verified = !$this->email_verified; break;
            case self::STATUS_IS_ACTIVE: $this->is_active = !$this->is_active; break;
            case self::STATUS_IS_DISABLED: $this->is_disabled = !$this->is_disabled; break;
        }
        return $this->save(false);
    }

    public function isActive()
    {
        return (bool) $this->is_active;
    }

    public function isDisabled()
    {
        return (bool) $this->is_disabled;
    }

    public function isVerified()
    {
        return (bool) $this->email_verified;
    }

    public function saveLogin(){
        $loginClass = Yii::$app->controller->module->userLoginAttemptModel;
        $attempt = new $loginClass;
        $attempt->user_id = $this->id;
        $attempt->performed_on = date('Y-m-d H:i:s');
        $attempt->is_successful = 1;
        $attempt->ipv4 = Yii::$app->getRequest()->getUserIP();
        $attempt->user_agent = Yii::$app->getRequest()->getUserAgent();
        $attempt->insert();
    }

    public function attributeLabels() {
        return array_merge(parent::attributeLabels(),
            [
                'role'=>Yii::t('user_model', 'Role')
            ]
        );
    }

    public function getAuthAssignment()
    {
        return $this->hasOne(\hoopy1986\user\models\AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function getRole()
    {
        $role = \hoopy1986\user\models\AuthAssignment::findOne(['user_id' => $this->id]);
        if($role)
            return $role->item_name;
        return null;
    }
} 