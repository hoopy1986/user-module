<?php


namespace hoopy1986\user\components\traits;


trait BaseUserScenarioTrait {
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['email'], 'email'],
                [['username', 'email', 'lastname', 'firstname'], 'required', 'on' => 'create'],
            ]
        );
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['username', 'email', 'lastname', 'firstname'];
        return $scenarios;
    }
} 