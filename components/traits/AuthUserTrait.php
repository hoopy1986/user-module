<?php

namespace hoopy1986\user\components\traits;

use Yii;

trait AuthUserTrait {

    public function setRole($role){
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);
        if($role !== '')
            $auth->assign($auth->getRole($role), $this->id);
    }
} 