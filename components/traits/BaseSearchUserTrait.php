<?php

namespace hoopy1986\user\components\traits;

use \Yii;
use yii\data\ActiveDataProvider;
use yii\validators\SafeValidator;

trait BaseSearchUserTrait {
    public $role;

    public function search($params)
    {
        $user = Yii::$app->controller->module->userModel;
        $query = $user::find();
        $query->joinWith('authAssignment');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['role'] = [
            'asc' => ['auth_assignment.item_name' => SORT_ASC],
            'desc' => ['auth_assignment.item_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
                'id' => $this->id,
                'created_on' => $this->created_on,
                'updated_on' => $this->updated_on,
                'last_visited_on' => $this->last_visited_on,
                'password_set_on' => $this->password_set_on,
                'email_verified' => $this->email_verified,
                'is_active' => $this->is_active,
                'is_disabled' => $this->is_disabled,
            ]);

        $query->andFilterWhere(['like', 'username', $this->username])
              ->andFilterWhere(['like', 'password', $this->password])
              ->andFilterWhere(['like', 'email', $this->email])
              ->andFilterWhere(['like', 'firstname', $this->firstname])
              ->andFilterWhere(['like', 'lastname', $this->lastname])
              ->andFilterWhere(['like', 'auth_key', $this->auth_key])
              ->andFilterWhere(['like', 'activation_key', $this->activation_key])
              ->andFilterWhere(['like', 'access_token', $this->access_token])
              ->andFilterWhere(['like', 'auth_assignment.item_name', $this->role]);


        return $dataProvider;
    }
} 