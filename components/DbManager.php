<?php

namespace hoopy1986\user\components;

use yii\rbac\Item;
use yii\db\Query;

class DbManager extends \yii\rbac\DbManager{

    public function getRolesByRoleNotRecursive($roleName)
    {
        $childrenList = $this->getChildrenList();
        $result = [];
        $this->getChildrenFirstLevel($roleName, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where([
                'type' => Item::TYPE_ROLE,
                'name' => array_keys($result),
            ]);
        $roles = [];
        foreach ($query->all($this->db) as $row) {
            $roles[$row['name']] = $this->populateItem($row);
        }
        return $roles;
    }

    public function getPermissionsByRoleNotRecursive($roleName)
    {
        $childrenList = $this->getChildrenList();
        $result = [];
        $this->getChildrenFirstLevel($roleName, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where([
                'type' => Item::TYPE_PERMISSION,
                'name' => array_keys($result),
            ]);
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }

    private function getChildrenFirstLevel($name, $childrenList, &$result)
    {
        if (isset($childrenList[$name])) {
            foreach ($childrenList[$name] as $child) {
                $result[$child] = true;
            }
        }
    }
}
