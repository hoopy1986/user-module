<?php


namespace hoopy1986\user\components\interfaces;

interface BaseUserInterface extends \yii\web\IdentityInterface{

    const STATUS_EMAIL_VERIFIED = 'email_verified';
    const STATUS_IS_ACTIVE = 'is_active';
    const STATUS_IS_DISABLED = 'is_disabled';

    public static function findIdentity($id);
    public static function findByUsername($username);
    public static function findByEmail($email);
    public static function findIdentityByAccessToken($token, $type = null);
    public function getId();
    public function getLocalizedUsername();
    public function getAuthKey();
    public function validateAuthKey($authKey);
    public function validatePassword($password);
    public function getActivationKey();
    public function verifyActivationKey($activationKey);
    public function authenticate();
    public function setPassword($password, $save = false);
    public function sendEmail($mode);
    public function toggleStatus($status);
    public function isVerified();
    public function isActive();
    public function isDisabled();
    public function saveLogin();
}