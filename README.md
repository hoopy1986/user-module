User module
================================

Config
------------

```json
'modules' => array(
        'user' => [
            'class' => 'hoopy1986\user\Module',
        ]
    ),
```


Minden beállítható ami a Module.php-ban van. Nagyjából hasonlóan, mint ahogy itt látható:

```json
'modules' => [
        'user' => [
			'class' => 'hoopy1986\user\Module',
			'defaultRoute' => 'users',
			'userMailEndpoint' => 'http://domain.com/user/site/',
			'userProfileAttached' => [
				'login' => [
					'searchModel' => 'app\models\search\SearchUserLoginAttempt'
				],
				'extra' => [
					'label' => Yii::t('user_controller_user', 'Extra'),
					'type' => 'detailView',
					'model' => '\app\models\TempUserExtra',
					'view' => '@app/views/users/extraView',
					'update' => '@app/views/users/extraUpdate',
				],
			],
			'views' => [
				'layout' => '@hoopy1986/admin/views/layouts/main',
				'site' => [
					'layout' => '@hoopy1986/admin/views/layouts/wrapper-black',
					'login' => '@app/views/site/login',
					'recover' => '@app/views/site/recover',
					'recovery' => '@app/views/site/recovery',
					'register' => '@app/views/site/register'
				],
				'users' => [
					//'index' => '@hoopy1986/admin/views/users/index',
					//'update' => '@hoopy1986/admin/views/users/update',
					//'view' => '@hoopy1986/admin/views/users/view'
				]
			],
    ],
]

'components' => [
	...
	'userMailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'viewPath' => '@hoopy1986/user/views/emails',
                'messageConfig' => [
                    'from' => 'user@domain.com',
                ],
                'useFileTransport' => true,
            ],
	...
]

```

User model:

```php
class User extends \app\models\base\BaseUser implements
    \hoopy1986\user\components\interfaces\BaseUserInterface,
    \hoopy1986\user\components\interfaces\AuthUserInterface
{

    use \hoopy1986\user\components\traits\BaseUserTrait;
    use \hoopy1986\user\components\traits\BaseUserScenarioTrait;
    use \hoopy1986\user\components\traits\AuthUserTrait;



    //public function afterLogin(){}
    //public function beforeLogout(){}
    //public function afterAdminCreate(){};
}
```

User model:

```php
class UserLoginAttempt extends \app\models\base\BaseUserLoginAttempt
{
}
```


