<?php

use yii\rbac\Item;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::t('authorizement', 'Roles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('authorizement', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-view">
    <p>
        <?= Html::a(
                Yii::t('authorizement', 'Update'),
                    ['update', 'id' => $model->name],
                    ['class' => 'btn btn-primary']
        ) ?>
        <?php
        echo Html::a(
                 Yii::t('authorizement', 'Delete'),
                     ['delete', 'id' => $model->name],
                     [
                         'class' => 'btn btn-danger',
                         'data-confirm' => Yii::t('authorizement', 'Are you sure to delete this item?'),
                         'data-method' => 'post',
                     ]
        );
        ?>
    </p>
    <?php
    echo DetailView::widget(
       [
           'model' => $model,
           'attributes' => [
               'name',
               'description:ntext',
               'ruleName',
               'data:ntext',
           ],
       ]
    );
    echo \hoopy1986\user\widgets\DualListBox::widget(
        [
            'id' => 'RoleHolder',
            'multiArray' => true,
            'unSelectedElements' => $roles,
            'selectedElements' => $attachedRoles,
            'height' => '180px',
            'addUrl' => \Yii::$app->getUrlManager()->createUrl(['user/roles/addchild/', 'id' => $model->name, 'type' => Item::TYPE_ROLE]),
            'removeUrl' => \Yii::$app->getUrlManager()->createUrl(['user/roles/removechild/', 'id' => $model->name, 'type' => Item::TYPE_ROLE]),
            'selectedTitle' => Yii::t('user_controller_route', 'Selected subroles'),
            'unSelectedTitle' => Yii::t('user_controller_route', 'Available subroles'),
        ]
    );
    echo \hoopy1986\user\widgets\DualListBox::widget(
        [
            'id' => 'RouteHolder',
            'multiArray' => true,
            'unSelectedElements' => $routes,
            'selectedElements' => $attachedRoutes,
            'height' => '180px',
            'addUrl' => \Yii::$app->getUrlManager()->createUrl(['user/roles/addchild/', 'id' => $model->name]),
            'removeUrl' => \Yii::$app->getUrlManager()->createUrl(['user/roles/removechild/', 'id' => $model->name]),
            'selectedTitle' => Yii::t('user_controller_route', 'Selected routes'),
            'unSelectedTitle' => Yii::t('user_controller_route', 'Available routes'),
        ]
    );
    echo \hoopy1986\user\widgets\DualListBox::widget(
        [
            'id' => 'PermissionHolder',
            'multiArray' => true,
            'unSelectedElements' => $permissions,
            'selectedElements' => $attachedPermissions,
            'height' => '180px',
            'addUrl' => \Yii::$app->getUrlManager()->createUrl(['user/roles/addchild/', 'id' => $model->name]),
            'removeUrl' => \Yii::$app->getUrlManager()->createUrl(['user/roles/removechild/', 'id' => $model->name]),
            'selectedTitle' => Yii::t('user_controller_route', 'Selected permissions'),
            'unSelectedTitle' => Yii::t('user_controller_route', 'Available permissions'),
        ]
    );
    ?>
</div>