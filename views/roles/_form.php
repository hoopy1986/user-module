<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div id="create-role-box">
    <?php $form = ActiveForm::begin([
            'id' => 'create-role-form',
            'enableClientValidation' => false,
            'layout' => 'horizontal'
        ]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
    <?=
    $form->field($model, 'rule_name')->widget('yii\jui\AutoComplete', [
            'options' => [
                'class' => 'form-control',
            ],
            'clientOptions' => [
                'source' => array_keys(Yii::$app->authManager->getRules()),
            ]
        ])
    ?>
    <?= $form->field($model, 'data')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-6">
            <?php
            echo Html::submitButton($model->isNewRecord ? Yii::t('authorizement', 'Create') : Yii::t('authorizement', 'Update'), [
                    'class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block'])
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>