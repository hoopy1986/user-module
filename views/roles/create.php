<?php
use yii\helpers\Html;

$this->title = Yii::t('authorizement', 'Create Role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('authorizement', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-create">
    <?php echo $this->render('_form', [
            'model' => $model,
        ]); ?>
</div>