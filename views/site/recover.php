<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('user_controller_site','Set new password');
?>

<div class="form-box" id="login-box">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('user_controller_site','Please fill out the following fields to set new password:'); ?></p>

    <?php $form = ActiveForm::begin([
            'id' => 'password-form',
            'enableClientValidation' => false,
            'options' => ['class' => 'form-horizontal']
        ]); ?>

    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-10">
            <?= Html::submitButton(Yii::t('user_controller_site','Set new password'), ['class' => 'btn btn-primary btn-block', 'name' => 'password-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>