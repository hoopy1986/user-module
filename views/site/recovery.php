<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('user_controller_site','Recover password');
?>

<div class="form-box" id="login-box">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('user_controller_site','Enter your username or email address'); ?></p>

    <?php $form = ActiveForm::begin([
            'id' => 'recovery-form',
            'enableClientValidation' => false,
            'options' => ['class' => 'form-horizontal']
        ]); ?>

    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-10">
            <?= Html::submitButton(Yii::t('user_controller_site','Send'), ['class' => 'btn btn-primary btn-block', 'name' => 'recovery-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>