<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('user_controller_site','Login');
?>

<div class="form-box" id="login-box">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('user_controller_site','Please fill out the following fields to login:'); ?></p>

    <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableClientValidation' => false,
            'options' => ['class' => 'form-horizontal']
        ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe', [
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ])->checkbox() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-10">
            <?= Html::submitButton(Yii::t('user_controller_site','Login'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>
    </div>
    <div class="form-group">
        <?php if(Yii::$app->controller->module->allowRegistration): ?>
            <div class="col-lg-offset-1 col-lg-5">
                <?= Html::a(Yii::t('user_controller_site','Register'), ['/user/site/register'], ['class' => 'btn btn-success btn-block', 'name' => 'register-button']) ?>
            </div>
            <div class="col-lg-5">
                <?= Html::a(Yii::t('user_controller_site','Recover password'), ['/user/site/recovery'], ['class' => 'btn btn-warning btn-block', 'name' => 'recovery-button']) ?>
            </div>
        <?php else : ?>
            <div class="col-lg-offset-1 col-lg-10">
                <?= Html::a(Yii::t('user_controller_site','Recover password'), ['/user/site/recovery'], ['class' => 'btn btn-warning btn-block', 'name' => 'recovery-button']) ?>
            </div>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>