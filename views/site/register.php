<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('user_controller_site','Register');
?>

<div class="form-box" id="login-box">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('user_controller_site','Please fill out the following fields to register:'); ?></p>

    <?php $form = ActiveForm::begin([
            'id' => 'register-form',
            'enableClientValidation' => false,
            'options' => ['class' => 'form-horizontal']
        ]); ?>

    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-10">
            <?= Html::submitButton(Yii::t('user_controller_site','Register'), ['class' => 'btn btn-primary btn-block', 'name' => 'register-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>