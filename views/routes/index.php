<?php

use yii\widgets\Pjax;
use hoopy1986\user\components\CustomHtml;

$this->title = Yii::t('user_controller_route', 'Routes');
$this->params['breadcrumbs'][] = $this->title;

echo CustomHtml::ajaxA(
                  Yii::t('user_controller_route', 'Refresh routes'),
                      ['routesearch'],
                      [
                          'class' => 'btn btn-success',
                          'data-method' => 'post',
                          'containerID' => 'pjax-routes-holder'
                      ]
        );
Pjax::begin(['id' => 'pjax-routes-holder']);
echo \hoopy1986\user\widgets\DualListBox::widget(
                                        [
                                            'id' => 'RouteHolder',
                                            'unSelectedElements' => $routes,
                                            'selectedElements' => $exists,
                                            'addUrl' => \Yii::$app->getUrlManager()->createUrl(['user/routes/add']),
                                            'removeUrl' => \Yii::$app->getUrlManager()->createUrl(['user/routes/remove']),
                                            'selectedTitle' => Yii::t('user_controller_route', 'Selected routes'),
                                            'unSelectedTitle' => Yii::t('user_controller_route', 'Available routes'),
                                        ]
);
Pjax::end();
