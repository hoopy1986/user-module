<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

$this->title = $model->getLocalizedUsername();
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_controller_user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->getLocalizedUsername();
?>
<div class="user-update">
    <?=
    Tabs::widget(
        [
            'items' => $items,
        ]
    );
    ?>
</div>