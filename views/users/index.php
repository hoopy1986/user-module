<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use hoopy1986\user\components\CustomHtml;
use kartik\editable\Editable;

$this->title = Yii::t('user_controller_user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
    <?php if(Yii::$app->controller->module->adminCanCreateUser) : ?>
    <p>
        <?= Html::a(Yii::t('user_controller_user', 'Create user'), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    <?php endif; ?>
    <?php Pjax::begin(['id' => 'pjax-users-gridview']) ?>
    <?=
    GridView::widget(
            [
                'id' => 'users-gridview',
                'dataProvider' => $dataProvider,
                'filterModel' => $model,
                'columns' => [
                    'username:text:' . Yii::t('user_controller_user', 'Username'),
                    'email:text:' . Yii::t('user_controller_user', 'Email'),
                    'firstname:text:' . Yii::t('user_controller_user', 'Firstname'),
                    'lastname:text:' . Yii::t('user_controller_user', 'Lastname'),
                    [
                        'attribute' => 'created_on',
                        'format' => ['date', 'php:Y-m-d H:i:s'],
                        'label' => Yii::t('user_controller_user', 'Created')
                    ],
                    /*[
                        'attribute' => 'updated_on',
                        'format' => ['date', 'php:Y-m-d H:i:s'],
                        'label' => Yii::t('user_controller_user', 'Updated')
                    ],*/
                    [
                        'attribute' => 'last_visited_on',
                        'format' => ['date', 'php:Y-m-d H:i:s'],
                        'label' => Yii::t('user_controller_user', 'Last visited')
                    ],
                    /*[
                        'attribute' => 'password_set_on',
                        'format' => ['date', 'php:Y-m-d H:i:s'],
                        'label' => Yii::t('user_controller_user', 'Password set')
                    ],*/
                    [
                        'label' => Yii::t('user_controller_user', 'Role'),
                        'attribute' => 'role',
                        'format' => 'raw',
                        'value' => function($data){
                                return Editable::widget([
                                        'name' => 'role',
                                        'value' => $data->role,
                                        'type'=>'primary',
                                        'size'=>'sm',
                                        'inputType'=>'dropDownList',
                                        'pjaxContainerId'=>'pjax-users-gridview',
                                        'data' => \hoopy1986\user\models\AuthItem::findAllRole(),
                                        'formOptions' => [
                                            'action' => array("setrole", "id" => $data->id),
                                        ],
                                        'pluginEvents' => [
                                            "editableSubmit" => "function(event, val, form) { $.pjax.reload({container:\"#pjax-users-gridview\"});}"
                                        ]
                                    ]);
                            }
                    ],
                    [
                        'attribute' => 'email_verified',
                        'format' => 'raw',
                        'label' => Yii::t('user_controller_user', 'Verified'),
                        'filter' => ['0' => Yii::t('user_controller_user', 'No'), '1' => Yii::t('user_controller_user', 'Yes')],
                        'value' => function ($data) {
                                return CustomHtml::ajaxA(
                                           $data->isVerified() ? Yii::t("user_controller_user", "Yes") : Yii::t("user_controller_user", "No"),
                                               array("verify", "id" => $data->id),
                                               array("class" => "actionButton", "title" => Yii::t("user_controller_user", "Toggle"), 'containerID' => 'pjax-users-gridview')
                                );
                            },
                    ],
                    [
                        'attribute' => 'is_active',
                        'format' => 'raw',
                        'label' => Yii::t('user_controller_user', 'Activated'),
                        'filter' => ['0' => Yii::t('user_controller_user', 'No'), '1' => Yii::t('user_controller_user', 'Yes')],
                        'value' => function ($data) {
                                return CustomHtml::ajaxA(
                                           $data->isActive() ? Yii::t("user_controller_user", "Yes") : Yii::t("user_controller_user", "No"),
                                               array("activate", "id" => $data->id),
                                               array("class" => "actionButton", "title" => Yii::t("user_controller_user", "Toggle"), 'containerID' => 'pjax-users-gridview')
                                );
                            },
                    ],
                    [
                        'attribute' => 'is_disabled',
                        'format' => 'raw',
                        'label' => Yii::t('user_controller_user', 'Disabled'),
                        'filter' => ['0' => Yii::t('user_controller_user', 'No'), '1' => Yii::t('user_controller_user', 'Yes')],
                        'value' => function ($data) {
                                return CustomHtml::ajaxA(
                                           $data->isDisabled() ? Yii::t("user_controller_user", "Yes") : Yii::t("user_controller_user", "No"),
                                               array("disable", "id" => $data->id),
                                               array("class" => "actionButton", "title" => Yii::t("user_controller_user", "Toggle"), 'containerID' => 'pjax-users-gridview')
                                );
                            },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}',
                        'contentOptions'=>['style'=>'min-width: 50px;'],
                        'buttons' => [
                            'view' => function($url, $mode){
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'a']).'&nbsp;';
                                },
                            'update' => function($url, $mode){
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => 'a']);
                                }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                                $urlPrefix = Yii::$app->controller->module->urlPrefix;
                                switch ($action){
                                    case 'view':
                                        $url = Yii::$app->controller->module->userViewProfileLink.'?id='.$model->id;
                                        return $url;
                                        break;
                                    case 'update':
                                        $url = Yii::$app->controller->module->userUpdateProfileLink.'?id='.$model->id;
                                        return $url;
                                        break;
                                }
                            }
                    ]
                ]
            ]
    );
    ?>
    <?php Pjax::end() ?>
</div>