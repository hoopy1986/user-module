<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div style="margin-top:20px;">
    <?php $form = ActiveForm::begin([
            'id' => 'profile-form',
            'enableClientValidation' => false,
            'layout' => 'horizontal'
        ]); ?>

    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'firstname') ?>
    <?= $form->field($model, 'lastname') ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::hiddenInput('modelName', $name); ?>
            <?= Html::hiddenInput('profile', 'true'); ?>
            <?= Html::submitButton(Yii::t('user_controller_user','Save'), ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>