<?php



use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('user_controller_user', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_controller_user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('user_controller_user', 'Create User');
?>

<div id="create-user-box">
    <?php $form = ActiveForm::begin([
            'id' => 'create-user-form',
            'enableClientValidation' => false,
            'layout' => 'horizontal'
        ]); ?>

    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'firstname') ?>
    <?= $form->field($model, 'lastname') ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-6">
            <?= Html::submitButton(Yii::t('user_controller_user','Create user'), ['class' => 'btn btn-primary btn-block', 'name' => 'create-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>