<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\widgets\Pjax;
use hoopy1986\user\components\CustomHtml;

$this->title = $model->getLocalizedUsername() . " (" . $model->username . ")";
$this->params['breadcrumbs'][] = ['label' => Yii::t('user_controller_user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->getLocalizedUsername();
?>
<div class="user-view">
    <p>
        <?php Pjax::begin(['id' => 'pjax-user-view']) ?>
        <?=
        Html::a(
            Yii::t('user_controller_user', 'Update'),
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']
        ) ?>
        <?=
        CustomHtml::ajaxA(
                  $model->isActive() ? Yii::t('user_controller_user', 'Active') : Yii::t('user_controller_user', 'Inactive'),
                      ['activate', 'id' => $model->id],
                      [
                          'class' => $model->isActive() ? 'btn btn-success' : 'btn btn-danger',
                          'data-method' => 'post',
                          'containerID' => 'pjax-user-view'
                      ]
        );
        ?>
        <?=
        CustomHtml::ajaxA(
                  $model->isVerified() ? Yii::t('user_controller_user', 'Verified') : Yii::t('user_controller_user', 'Unverified'),
                      ['verify', 'id' => $model->id],
                      [
                          'class' => $model->isVerified() ? 'btn btn-success' : 'btn btn-danger',
                          'data-method' => 'post',
                          'containerID' => 'pjax-user-view'
                      ]
        );
        ?>
        <?=
        CustomHtml::ajaxA(
                  $model->isDisabled() ? Yii::t('user_controller_user', 'Disabled') : Yii::t('user_controller_user', 'Enabled'),
                      ['disable', 'id' => $model->id],
                      [
                          'class' => $model->isDisabled() ? 'btn btn-danger' : 'btn btn-success',
                          'data-method' => 'post',
                          'containerID' => 'pjax-user-view'
                      ]
        );
        ?>
    </p>
<?=
Tabs::widget(
    [
        'items' => $items,
    ]
);
?>
<?php Pjax::end() ?>
    </div>