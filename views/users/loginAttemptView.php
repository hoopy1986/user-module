<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

Pjax::begin(['id' => 'pjax-profile-gridview'])
?>
<?= GridView::widget(
             [
                 'id' => 'profile-gridview',
                 'dataProvider' => $dataProvider,
                 'filterModel' => $model,
                 'columns' => [
                     [
                         'attribute' => 'performed_on',
                         'format' => ['date', 'php:Y-m-d H:i:s'],
                         'label' => Yii::t('user_controller_user', 'Date')
                     ],
                     [
                         'attribute' => 'ipv4',
                         'format' => 'text',
                         'label' => Yii::t('user_controller_user', 'IP address')
                     ],
                     [
                         'attribute' => 'user_agent',
                         'format' => 'text',
                         'label' => Yii::t('user_controller_user', 'User agent')
                     ],
                 ]
             ]);?>
<?php Pjax::end() ?>