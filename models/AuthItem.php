<?php

namespace hoopy1986\user\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auth_item".
 */
class AuthItem extends \hoopy1986\user\models\base\BaseAuthItem
{
    public static function findAllRole()
    {
        return ArrayHelper::merge(['' => Yii::t('user_controller_user', 'Unset')], ArrayHelper::map(self::findAll(['type' => '1']), 'name', 'name'));
    }
}
