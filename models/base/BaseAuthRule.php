<?php

namespace hoopy1986\user\models\base;

use Yii;

/**
 * This is the base-model class for table "auth_rule".
 *
 * @property string $name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \hoopy1986\user\models\AuthItem[] $authItems
 */
class BaseAuthRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['data'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('authorization', 'Name'),
            'data' => Yii::t('authorization', 'Data'),
            'created_at' => Yii::t('authorization', 'Created At'),
            'updated_at' => Yii::t('authorization', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItems()
    {
        return $this->hasMany(\hoopy1986\user\models\AuthItem::className(), ['rule_name' => 'name']);
    }
}
