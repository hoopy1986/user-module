<?php

namespace hoopy1986\user\models\base;

use Yii;

/**
 * This is the base-model class for table "auth_item_child".
 *
 * @property string $parent
 * @property string $child
 *
 * @property \hoopy1986\user\models\AuthItem $parent0
 * @property \hoopy1986\user\models\AuthItem $child0
 */
class BaseAuthItemChild extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item_child';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent' => Yii::t('authorization', 'Parent'),
            'child' => Yii::t('authorization', 'Child'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(\hoopy1986\user\models\AuthItem::className(), ['name' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChild0()
    {
        return $this->hasOne(\hoopy1986\user\models\AuthItem::className(), ['name' => 'child']);
    }
}
