<?php

namespace hoopy1986\user\models\base;

use Yii;

/**
 * This is the base-model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \hoopy1986\user\models\AuthAssignment[] $authAssignments
 * @property \hoopy1986\user\models\User[] $users
 * @property \hoopy1986\user\models\AuthRule $ruleName
 * @property \hoopy1986\user\models\AuthItemChild[] $authItemChildren
 */
class BaseAuthItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['rule_name'], 'default']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('authorization', 'Name'),
            'type' => Yii::t('authorization', 'Type'),
            'description' => Yii::t('authorization', 'Description'),
            'rule_name' => Yii::t('authorization', 'Rule Name'),
            'data' => Yii::t('authorization', 'Data'),
            'created_at' => Yii::t('authorization', 'Created At'),
            'updated_at' => Yii::t('authorization', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(\hoopy1986\user\models\AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(\hoopy1986\user\models\User::className(), ['id' => 'user_id'])->viaTable('auth_assignment', ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(\hoopy1986\user\models\AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(\hoopy1986\user\models\AuthItemChild::className(), ['child' => 'name']);
    }
}
