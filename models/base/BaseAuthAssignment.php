<?php

namespace hoopy1986\user\models\base;

use Yii;

/**
 * This is the base-model class for table "auth_assignment".
 *
 * @property string $item_name
 * @property integer $user_id
 * @property integer $created_at
 *
 * @property \hoopy1986\user\models\AuthItem $itemName
 * @property \hoopy1986\user\models\User $user
 */
class BaseAuthAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['item_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('authorization', 'Item Name'),
            'user_id' => Yii::t('authorization', 'User ID'),
            'created_at' => Yii::t('authorization', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemName()
    {
        return $this->hasOne(\hoopy1986\user\models\AuthItem::className(), ['name' => 'item_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\hoopy1986\user\models\User::className(), ['id' => 'user_id']);
    }
}
