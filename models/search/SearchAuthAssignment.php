<?php

namespace hoopy1986\user\models\search;

use Yii;
use hoopy1986\user\models\AuthAssignment;
use yii\data\ActiveDataProvider;

/**
 * SearchAuthAssignment represents the model behind the search form about `AuthAssignment`.
 */
class SearchAuthAssignment extends AuthAssignment

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name'], 'safe'],
            [['user_id', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return AuthAssignment::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuthAssignment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name]);

        return $dataProvider;
    }
}