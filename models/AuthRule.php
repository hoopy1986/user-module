<?php

namespace hoopy1986\user\models;

use Yii;

/**
 * This is the model class for table "auth_rule".
 */
class AuthRule extends \hoopy1986\user\models\base\BaseAuthRule
{
    public static function findAllRule()
    {
        return ArrayHelper::map(self::findAll(['type' => '1']), 'name', 'name');
    }
}
