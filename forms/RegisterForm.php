<?php

namespace hoopy1986\user\forms;

use hoopy1986\user\Module;
use Yii;
use yii\base\Model;
use common\models;

class RegisterForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $passwordRepeat;

    public function rules()
    {
        return [
            [['username', 'email', 'password', 'passwordRepeat'], 'required'],
            [['email'], 'email'],
            ['password', 'string', 'length' => [Yii::$app->controller->module->passwordMinLength, Yii::$app->controller->module->passwordMaxLength]],
            ['password', 'passwordStrength', 'params' => ['level' => Yii::$app->controller->module->passwordStrength]],
            ['passwordRepeat', 'comparePassword'],
            [['username'], 'vaildateUniqueUsername'],
            [['email'], 'vaildateUniqueEmail']
        ];
    }

    public function vaildateUniqueUsername($attribute, $params)
    {
        if(!$this->uniqueField('username'))
            $this->addError($attribute, Yii::t('user_forms','This username is already in use'));
    }

    public function vaildateUniqueEmail($attribute, $params)
    {
        if(!$this->uniqueField('email'))
            $this->addError($attribute, Yii::t('user_forms','This email address is already in use'));
    }

    public function comparePassword($attribute, $params)
    {
        if($this->password !== $this->passwordRepeat)
            $this->addError($attribute, Yii::t('user_forms','{attribute} must be repeated exactly.', ['attribute' => $this->getAttributeLabel('password')]));
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user_forms', 'Username'),
            'email' => Yii::t('user_forms', 'Email'),
            'password' => Yii::t('user_forms', 'Password'),
            'passwordRepeat' => Yii::t('user_forms', 'Verify password'),
        ];
    }

    public function uniqueField($type)
    {
        $userClass = Yii::$app->controller->module->userModel;
        switch($type){
            case 'username':
                if($userClass::findByUsername($this->username))
                    return false;
                break;
            case 'email':
                if($userClass::findByEmail($this->email))
                    return false;
                    break;
        }
        return true;
    }

    public function passwordStrength($attribute, $params)
    {
        $level = 0;
        $levelPatterns = array(
            '/^(?=.*[0-9])(?=\S+$).{0,}$/',
            '/^(?=.*[a-z])(?=\S+$).{0,}$/',
            '/^(?=.*[A-Z])(?=\S+$).{0,}$/',
            '/^(?=.*[!"#$%&()*+,-.:;<=>?@[\]^_`{|}~])(?=\S+$).{0,}$/',
        );
        foreach ($levelPatterns as $pattern) {
            if (preg_match($pattern, $this->$attribute))
                $level++;
        }
        if ($params['level'] > $level)
            $this->addError($attribute, Yii::t('user_forms','The password is not strength enough. It has to contain at least {level} different type of characters of these without whitespaces: (lower case alpha, upper case alpha, decimal digits, special characters)!', ['level' => $params['level']]));
    }

    public function registerUser(){
        $userClass = Yii::$app->controller->module->userModel;
        $user = new $userClass;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->insert();
        if(Yii::$app->controller->module->emailVerification){
            $user->sendEmail('verify');
        }
        if(Yii::$app->controller->module->activeUsersOnly){
            Yii::$app->session->setFlash('success', Yii::t('user_forms', 'Registration was successfull. Wait for activation email then You can log in.'));
        }
        else if(Yii::$app->controller->module->emailVerification){
            Yii::$app->session->setFlash('success', Yii::t('user_forms', 'Registration was successfull. Check your emails, to verify your address then You can log in.'));
        }
        else if(Yii::$app->controller->module->activeUsersOnly && Yii::$app->controller->module->emailVerification){
            Yii::$app->session->setFlash('success', Yii::t('user_forms', 'Registration was successfull. Check your emails, to verify your address and wait for activation email then You can log in.'));
        }else{
            Yii::$app->session->setFlash('success', Yii::t('user_forms', 'Registration was successfull. Now You can log in.'));
        }
    }
}