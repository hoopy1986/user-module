<?php

namespace hoopy1986\user\forms;

use hoopy1986\user\Module;
use Yii;
use yii\base\Model;
use common\models;

class PasswordForm extends Model
{
    public $password;
    public $passwordRepeat;

    public function rules()
    {
        return [
            [['password', 'passwordRepeat'], 'required'],
            ['password', 'string', 'length' => [Yii::$app->controller->module->passwordMinLength, Yii::$app->controller->module->passwordMaxLength]],
            ['password', 'passwordStrength', 'params' => ['level' => Yii::$app->controller->module->passwordStrength]],
            ['passwordRepeat', 'comparePassword'],
        ];
    }

    public function comparePassword($attribute, $params)
    {
        if($this->password !== $this->passwordRepeat)
            $this->addError($attribute, Yii::t('user_forms','{attribute} must be repeated exactly.', ['attribute' => $this->getAttributeLabel('password')]));
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('user_forms', 'Password'),
            'passwordRepeat' => Yii::t('user_forms', 'Verify password'),
        ];
    }

    public function passwordStrength($attribute, $params)
    {
        $level = 0;
        $levelPatterns = array(
            '/^(?=.*[0-9])(?=\S+$).{0,}$/',
            '/^(?=.*[a-z])(?=\S+$).{0,}$/',
            '/^(?=.*[A-Z])(?=\S+$).{0,}$/',
            '/^(?=.*[!"#$%&()*+,-.:;<=>?@[\]^_`{|}~])(?=\S+$).{0,}$/',
        );
        foreach ($levelPatterns as $pattern) {
            if (preg_match($pattern, $this->$attribute))
                $level++;
        }
        if ($params['level'] > $level)
            $this->addError($attribute, Yii::t('user_forms','The password is not strength enough. It has to contain at least {level} different type of characters of these without whitespaces: (lower case alpha, upper case alpha, decimal digits, special characters)!', ['level' => $params['level']]));
    }
}