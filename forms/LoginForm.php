<?php

namespace hoopy1986\user\forms;

use hoopy1986\user\Module;
use Yii;
use yii\base\Model;
use common\models;

class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user_forms', 'Username'),
            'password' => Yii::t('user_forms', 'Password'),
            'rememberMe' => Yii::t('user_forms', 'Remember Me'),
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('user_forms','Incorrect username or password'));
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            $result = $this->getUser()->authenticate();
            if($result === true)
                return Yii::$app->user->login($this->_user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            else{
                $this->addError($result[0], $result[1]);
            }
        } else {
            return false;
        }
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $userClass = Yii::$app->controller->module->userModel;
            $this->_user = $userClass::findByUsername($this->username);
        }
        return $this->_user;
    }
}