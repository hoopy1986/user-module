<?php

namespace hoopy1986\user\forms;

use hoopy1986\user\Module;
use Yii;
use yii\base\Model;
use common\models;

class RecoveryForm extends Model
{
    public $username;
    public $email;

    private $user;

    public function rules()
    {
        return [
            [['username', 'email'], 'recoveryValidation', 'skipOnEmpty' => false],
            [['email'], 'email']
        ];
    }

    public function recoveryValidation($attribute, $params)
    {
        if($this->username == '' && $this->email == ''){
            $this->addError($attribute, Yii::t('user_forms','Fill one of the two fields.'));
        }else if($this->username != '' && $this->email != ''){
            $this->addError($attribute, Yii::t('user_forms','Fill only one of the two fields.'));
        }else{
            $userClass = Yii::$app->controller->module->userModel;
            if($this->username != ''){
                $this->user = $userClass::findByUsername($this->username);
                if(!$this->user){
                    if($attribute == 'username')
                        $this->addError($attribute, Yii::t('user_forms','No user with this username.'));
                }
            }else{
                $this->user = $userClass::findByEmail($this->email);
                if(!$this->user){
                    if($attribute == 'email')
                        $this->addError($attribute, Yii::t('user_forms','No user with this email address.'));
                }
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user_forms', 'Username'),
            'email' => Yii::t('user_forms', 'Email')
        ];
    }

    public function recoveryUser(){
        $this->user->sendEmail('recover');
        Yii::$app->session->setFlash('success', Yii::t('user_forms', 'Email with recovery instruction was sent to your email address.'));
    }
}